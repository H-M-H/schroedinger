#include <tuple>
#include <iostream>

#include <expr.h>
#include <lina.h>
#include <schroedinger.h>

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;
using namespace pybind11::literals;

std::tuple<Eigen::VectorXd, Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>>
pyschroedinger(std::string Pot, double a, double b, int neigs, int n, int maxit, double prec, int ncv)
{
	Eigen::VectorXd Eval;
	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> Evec;
	if (schroedinger(Pot, a, b, Eval, Evec, neigs, n, maxit, prec, ncv) != -1)
		return std::make_tuple(std::move(Eval), std::move(Evec));
	else
		throw std::runtime_error("No Eigenvalues found !");
}

PYBIND11_MODULE(pyschroedinger, m)
{
	m.doc() = "Numeric solutions for Schroedingers equation.";
	m.def(
		"schroedinger",
		&pyschroedinger,
		"magic",
		"Pot"_a,
		"a"_a,
		"b"_a,
		"neigs"_a = 50,
		"n"_a = 1000,
		"maxit"_a=1000,
		"prec"_a=1e-10,
		"ncv"_a = -1);

	py::class_<Expr<double>>(m, "Expr")
		.def(py::init<std::string>())
		.def("__call__", &Expr<double>::operator());
}
