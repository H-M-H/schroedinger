#ifndef SOLVER_H
#define SOLVER_H

#include <MatOp/SparseSymMatProd.h>
#include <SymEigsSolver.h>

#ifndef INSTANTIATE_SOLVER
extern template class Spectra::SparseSymMatProd<double>;
extern template class Spectra::SymEigsSolver<
	double,
	Spectra::SMALLEST_MAGN,
	Spectra::SparseSymMatProd<double>>;
#endif

#endif
