#define INSTANTIATE_SOLVER

#include "solver.h"

template class Spectra::SparseSymMatProd<double>;
template class Spectra::SymEigsSolver<
	double,
	Spectra::SMALLEST_MAGN,
	Spectra::SparseSymMatProd<double>>;
