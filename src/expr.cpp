#define EXPR_INSTANTIATE

#include <exprtk.hpp>

#include "expr.h"

template<typename T>
Expr<T>::Expr(std::string expr)
{
	typedef exprtk::symbol_table<T> symbol_table_t;
	typedef exprtk::parser<T>			 parser_t;

	m_pExpression = std::make_unique<expression_t>();

	symbol_table_t symbol_table;
	symbol_table.add_variable("x", m_X);

	m_pExpression->register_symbol_table(symbol_table);

	parser_t parser;
	if(!parser.compile(expr, *m_pExpression))
	{
		throw std::invalid_argument("'" + expr + "' is no valid expression.");
	}
}

template<typename T>
Expr<T>::~Expr() = default;

template<typename T>
T Expr<T>::operator () (T x)
{
	m_X = x;
	return m_pExpression->value();
}

template class Expr<double>;
