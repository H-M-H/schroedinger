#ifndef EXPR_H
#define EXPR_H

#include <memory>

namespace exprtk
{
	template<typename T>
	class expression;
}

template<typename T = double>
class Expr
{

public:
	Expr(std::string expr);
	~Expr();

	T operator () (T x);

private:
	typedef exprtk::expression<T> expression_t;

	T m_X;
	class std::unique_ptr<expression_t> m_pExpression;

};

#ifndef EXPR_INSTANTIATE
extern template class Expr<double>;
#endif

#endif
