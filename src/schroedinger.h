#ifndef SCHROEDINGER_H
#define SCHROEDINGER_H

#include <string>

#include "lina.h"

int schroedinger(
	std::string Pot,
	double a,
	double b,
	Eigen::VectorXd& Eval,
	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& Evec,
	int neigs = 50,
	int n = 1000,
	int maxit = 1000,
	double prec = 1e-10,
	int nvc = -1);

#endif
