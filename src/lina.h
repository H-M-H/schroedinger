#ifndef LINA_H
#define LINA_H

#include <Eigen/Core>
#include <Eigen/SparseCore>

#ifndef INSTANTIATE_LINA
extern template class Eigen::SparseMatrix<double>;
extern template class Eigen::
	Matrix<double, Eigen::Dynamic, 1, 0, -1, 1>; // Eigen::VectorXf
extern template class Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;
#endif

#endif
