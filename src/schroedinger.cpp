#include <cmath>

#include "expr.h"
#include "schroedinger.h"
#include "solver.h"

int schroedinger(
	std::string Pot,
	double a,
	double b,
	Eigen::VectorXd& Eval,
	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& Evec,
	int neigs,
	int n,
	int maxit,
	double prec,
	int ncv)
{
	if (n < 2)
		throw std::invalid_argument("n has to be bigger than 1 !");

	if (ncv == -1)
		ncv = (neigs + 2 * n) / 3.0 + 0.5;

	double Interval = b - a;

	if (Interval <= 0)
		throw std::invalid_argument("Negative Intervals are not allowed, "
									"choose other values for a and b !");

	double dx2 = pow(Interval / n, 2);

	Expr<> V(Pot);

	Eigen::SparseMatrix<double> H(n, n);
	H.reserve(3 * n - 2);

	for (int i = 0; i < n; i++)
	{
		H.insert(i, i) = 2.f / dx2 + V(a + Interval * i / (n - 1));
	}

	for (int i = 0; i < n - 1; i++)
	{
		H.insert(i + 1, i) = -1.f / dx2;
		H.insert(i, i + 1) = -1.f / dx2;
	}

	Spectra::SparseSymMatProd<double> Op(H);
	Spectra::SymEigsSolver<
		double,
		Spectra::SMALLEST_MAGN,
		Spectra::SparseSymMatProd<double>>
		Eig(&Op, neigs, ncv);

	Eig.init();

	int NConv = Eig.compute(maxit, prec, Spectra::SMALLEST_ALGE);

	if (Eig.info() == Spectra::SUCCESSFUL)
	{
		Eval = Eig.eigenvalues();
		Evec = Eig.eigenvectors() * std::sqrt((n - 1) / Interval);
		return NConv;
	}

	return -1;
}
