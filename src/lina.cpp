#define INSTANTIATE_EIGEN
#define EIGEN_NO_STATIC_ASSERT

#include "lina.h"

template class Eigen::SparseMatrix<double>;
template class Eigen::
	Matrix<double, Eigen::Dynamic, 1, 0, -1, 1>; // Eigen::VectorXf
template class Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

